# Mazzuma Playground

The Mazzuma Playground is a live Progressive Web Application, built to let you take the Mazzuma Payment API for a spin in a dynamic application. Check it out at [playground.khophi.tech](https://playground.khophi.tech)

Add your API Key from Mazzuma and test making payment requests in under 10 seconds.

The source code is available also for learning purposes. Clone this repository also to run this project on your local machine.

Watch the *Getting Started with Mazzuma YouTube series* today to get started with the platform, and integrate into your next application in no time.

## To Run Locally

Here are the steps to run this project locally

 - Clone this repository
 - Run `npm install` in project root directory (this step assumes you already have Node/NPM installed)

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Contributing

This project is extended and maintained using the Angular CLI, versin 7.0.4+. For consistency and easy maintainability, kindly use the CLI in generating the files and or modules needed, as shown below.

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## License

MIT