import { Component, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css']
})
export class HelpComponent implements OnInit {
  @ViewChild('myaccordion') myPanels;
  constructor(
    private title: Title
  ) { }

  ngOnInit() {
    this.title.setTitle('Help');
  }

  openAll() {
    this.myPanels.openAll();
  }

}
