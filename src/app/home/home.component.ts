import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ConfirmSendComponent } from './confirmsend.component';
import { SettingsService } from '../services/settings.service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  sender_name: string;
  price: string;
  sender_number: string;

  busy: boolean;

  constructor(private dialog: MatDialog, private settings: SettingsService,
    private router: Router,
    private title: Title,
    private snack: MatSnackBar) { }

  ngOnInit() {
    this.title.setTitle('Mazzuma Playground');
  }

  onSubmit(formData) {
    this.busy = true;

    const user_settings = this.settings.get() || {};

    if (!(user_settings.api_key || user_settings.recipient_number)) {
      this.busy = false;
      const showSnack =
      this.snack.open('Please visit the Settings page to add API Key and the recipient phone number account first', 'Take me there!');

      showSnack.afterDismissed()
        .subscribe((_) => {
          this.router.navigate(['/settings']);
        });

      return;
    }

    if (!Number.isInteger(parseInt(formData.price, 10))) {
      this.busy = false;
      return this.snack.open(`Oya! Enter number in the 'How much to send?' field`, 'Yes madam!');
    }

    let transaction_direction;
    let sender_network;

    const network_prefix = ['024', '054', '055', '026', '056', '027', '057'];

    if (!network_prefix.includes(formData.sender_number.slice(0, 3))) {
      return this.snack.open('The phone number entered is NOT a valid Ghana Mobile Network phone number.', 'Close');
    }

    const number_first_three_digits_sender = formData.sender_number.substring(0, 3);

    switch (number_first_three_digits_sender) {
      case '024' || '054' || '055':
        sender_network = 'mtn';
        break;
      case '026' || '056':
        sender_network = 'airtel';
        break;
      case '027' || '057':
        sender_network = 'tigo';
        break;
      default:
        this.snack.open('Ahem! This your phone number, is it a Ghana network?', 'Close');
    }

    const receive_network = this.settings.get().network_type;
    // set transaction direction

    // for from MTN to other networks
    if (receive_network === 'mtn' && sender_network === 'mtn') {
      transaction_direction = 'rmtm';
    } else if (receive_network === 'mtn' && sender_network === 'airtel') {
      transaction_direction = 'rmta';
    } else if (receive_network === 'mtn' && sender_network === 'tigo') {
      transaction_direction = 'rmtt';

      // from Airtel to other networks
    } else if (receive_network === 'airtel' && sender_network === 'airtel') {
      transaction_direction = 'rata';
    } else if (receive_network === 'airtel' && sender_network === 'mtn') {
      transaction_direction = 'ratm';
    } else if (receive_network === 'airtel' && sender_network === 'tigo') {
      transaction_direction = 'ratt';

      // from Tigo to other networks.
    } else if (receive_network === 'tigo' && sender_network === 'tigo') {
      transaction_direction = 'rttt';
    } else if (receive_network === 'tigo' && sender_network === 'airtel') {
      transaction_direction = 'rtta';
    } else if (receive_network === 'tigo' && sender_network === 'mtn') {
      transaction_direction = 'rttm';
    }

    console.log(transaction_direction);

    formData.option = transaction_direction;
    formData.recipient_number = user_settings.recipient_number;
    formData.sender_network = sender_network;
    // {
    //   "price": 1,
    //   "network": "mtn",
    //   "recipient_number": "026xxxxxxx",
    //   "sender": "024xxxxxxx",
    //   "option": "rmta",
    //   "apikey": "",
    //   "orderID": ""
    // }

    const dialogRef = this.dialog.open(ConfirmSendComponent, {
      width: '800px',
      data: formData,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(`The result data is: ${result}`);
    });
  }
}
