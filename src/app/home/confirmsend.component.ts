import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { PaymentService } from '../services/payment.service';
import { SettingsService } from '../services/settings.service';

@Component({
  selector: 'app-confirm-send',
  templateUrl: 'confirmsend.component.html',
  styleUrls: [],
})
export class ConfirmSendComponent {

  busy: boolean;

  constructor(
    public dialogRef: MatDialogRef<ConfirmSendComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private settings: SettingsService,
    private payment: PaymentService,
    private snack: MatSnackBar) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  makeTransaction(formData) {
    this.busy = true;
    console.log(formData);

    // {
    //   "code": "3",
    //   "status": "Forbidden",
    //   "message": "Invalid API-Key"
    // }

    this.payment.make_transaction(formData.price, formData.sender_network, formData.recipient_number,
      formData.sender_number, formData.option)
      .subscribe((res) => {
        this.busy = false;
        console.log(res);
        if (res['code'] === 1) {
          this.snack.open(`Hurray! ${res['message']}`, 'close');
          console.log('Transaction Succeeded');
          this.onNoClick();
        } else {
          this.snack.open(`Error Code: ${res['status']} - ${res['message']}`);
          console.log('Transaction Failed', res);
        }
      }, (error) => {
        this.busy = false;
        console.log(error);
        this.snack.open(JSON.stringify(error.error), 'close');
        this.onNoClick();
      });

  }
}
