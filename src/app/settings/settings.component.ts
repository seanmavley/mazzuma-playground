import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../services/settings.service';
import { MatSnackBar, MatCardTitle } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  settingsForm: FormGroup;
  theSettings: any;

  constructor(
    private settings: SettingsService,
    private router: Router,
    private title: Title,
    private snack: MatSnackBar,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.title.setTitle('Settings');
    this.settingsForm = this.fb.group({
      'api_key': '',
      'recipient_name': '',
      'recipient_number': '',
    });

    this.loadSettings();
  }

  loadSettings() {
    this.theSettings = this.settings.get() || {};
    this.settingsForm.patchValue({
      'api_key': this.theSettings.api_key || '',
      'recipient_name': this.theSettings.recipient_name || '',
      'recipient_number': this.theSettings.recipient_number || '',
    });
  }

  onSubmit(formData) {
    console.log(formData);
    const network_prefix = ['024', '054', '055', '026', '056', '027', '057'];

    if (!network_prefix.includes(formData.recipient_number.slice(0, 3))) {
      return this.snack.open('The phone number entered is NOT a valid Ghana Mobile Network phone number.', 'Close');
    }

    this.settings.save(formData);
    const mySnack = this.snack.open('Settings saved', 'Continue');
    this.loadSettings();
    mySnack.onAction()
      .subscribe((_) => {
        this.router.navigate(['/']);
      });
  }

  clearSettings() {
    this.settings.clear();
    this.snack.open('Settings cleared and reset', 'Close');
    this.loadSettings();
  }

}
