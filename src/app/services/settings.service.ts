import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  constructor(private snack: MatSnackBar) { }

  save(payLoad) {
    let network;

    const number_first_three_digits = payLoad.recipient_number.substring(0, 3);

    switch (number_first_three_digits) {
      case '024' || '054' || '055':
        network = 'mtn';
        break;
      case '026' || '056':
        network = 'airtel';
        break;
      case '027' || '057':
        network = 'tigo';
        break;
      default:
        this.snack.open('Ahem! This your phone number, is it a Ghana network?', 'Close');
    }

    payLoad.network_type = network;
    localStorage.setItem('settings', JSON.stringify(payLoad));
  }

  get() {
    return JSON.parse(localStorage.getItem('settings'));
  }

  clear() {
    localStorage.removeItem('settings');
  }
}
