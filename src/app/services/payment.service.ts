import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

const API_ENDPOINT = 'https://client.teamcyst.com/api_call.php';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(private http: HttpClient) { }

  // source: https://stackoverflow.com/a/105074/1757321
  guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  make_transaction(
    price: string,
    network: string,
    recipient_number: string,
    sender: string,
    option: string
  ) {
    const params = {
      'price': price,
      'network': network,
      'recipient_number': recipient_number,
      'sender': sender,
      'option': option || '',
      'apikey': JSON.parse(localStorage.getItem('settings')).api_key,
      'orderID': this.guid()
    };

    console.log(params);

    return this.http.post(API_ENDPOINT, params);

  }
}
